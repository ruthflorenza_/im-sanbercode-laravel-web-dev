@extends('layouts.master')

@section('title')
    Halaman List Game
@endsection

@push('scripts')
<script>
    Swal.fire({
        title: "Berhasil!",
        text: "Memasangkan script sweet alert",
        icon: "success",
        confirmButtonText: "Cool",
    });
</script>

@endpush

@section('content')

<a href="/game/create" class="btn btn-primary btn-sm-my-3">Tambah</a>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Action</th>

            </tr>
        </thead>
        <tbody>
            @forelse ($game as $key => $item)
                <tr>
                    <th scope="row">{{$key + 1}}</th>
                    <td>{{$item->name}}</td>
                    <td>
                        <form action="game/{{ $item->id }}" method="POST">
                            @csrf
                            @method('delete')
                            <a href="/game/{{ $item->id }}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/game/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                </tr>
            @empty
                <h2>Data Game Kosong </h2>
            @endforelse
            
        </tbody>
    </table>

@endsection