@extends('layouts.master')

@section('title')
    Halaman Detail Game
@endsection

@section('content')

<h1 class="text-primary">{{ $game->name}}</h1>
<p>Developer: {{ $game->developer }}</p>
<h3>Gameplay</h3>
<p>{{ $game->gameplay }}</p>
<h3>Platform</h3>

@foreach($platforms as $item)
    <span class="badge badge-pill badge-primary">{{ $item->name }}</span>
@endforeach
@endsection