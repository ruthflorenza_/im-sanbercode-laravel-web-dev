<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    
    <form method="POST" action="/welcome"><br>
        @csrf
        <label>First name:</label><br>
        <input type="text" name="first name"><br><br>
        <label>Last name:</label><br>
        <input type="text" name="last name"><br><br>
        <label>Gender:</label><br>
        <input type="radio" name="Male">Male<br>
        <input type="radio" name="Female">Female<br>
        <input type="radio" name="Other">other<br>
        <br>

        <label>Nationality</label><br>
            <select name="nationalities">
                <option value="indonesian">Indonesian</option>
                <option value="malaysia">Malaysia</option>
                <option value="singapura">Singapura</option>
                <option value="thailand">Thailand</option>
            </select>
        <br><br>

        <label>Language Spoken:</label><br>
        <input type="checkbox" name="Bahasa Indonesia">
        <label>Bahasa Indonesia</label><br>
        <input type="checkbox" name="English">
        <label>English</label><br>
        <input type="checkbox" name="Other">
        <label>Other</label>
        <br><br>

        <label>Bio:</label><br>
        <textarea name="message" rows="10" cols="30"></textarea>
        <br><br>
        <input type="submit" value="submit">
    </form>
</body>
</html>