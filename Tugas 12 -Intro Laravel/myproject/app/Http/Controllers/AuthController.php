<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form()
    {
        return view ('register');
    }

    public function tampil()
    {
        return view ('welcome');
    }

    public function daftar()
    {
        return view ('register');
    }

    public function masuk(Request $request)
    {
        $firstName = $request->input('first_name');
        $lastName = $request->input('last_name');

        return view ('welcome',["firstName" => $firstName, "lastName" => $lastName]);
    }

}
