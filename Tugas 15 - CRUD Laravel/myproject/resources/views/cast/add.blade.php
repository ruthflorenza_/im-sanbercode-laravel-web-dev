@extends('layouts.master')

@section('title')
Halaman Tambah Data Pemain Film 
@endsection

@section('content')

<form action="/cast" method="post">
    @csrf
  <div class="form-group"> 
    <label>Nama Pemain</label>
    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror">
  </div>
    @error('name')
        <div class="alert-danger">{{ $message }}</div>
    @enderror

  <div class="form-group">
    <label>Age</label>
    <input type="text" name="age" class="form-control @error('age') is-invalid @enderror">
  </div>
    @error('age')
        <div class="alert-danger">{{ $message }}</div>
    @enderror

  <div class="form-group">
    <label>Bio</label>
    <textarea name="bio" class="form-control @error('bio') is-invalid @enderror" cols="30" rows="10"></textarea>
  </div>
    @error('bio')
        <div class="alert-danger">{{ $message }}</div>
    @enderror

  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection