<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/home', [HomeController::class, 'utama' ]);

Route::get('/register', [AuthController::class, 'form']);

Route::get('/signup', [AuthController::class, 'daftar']);

Route::get('/welcome', [AuthController::class, 'tampil']);

Route::post('/welcome', [AuthController::class, 'masuk']);

Route::get('/data-tables', function() {
    return view('page.data-table');
});

Route::get('/tables', function(){
    return view('page.table');
});

//CRUD

// C => Create data
//Route mengarahkan untuk membuat data pemain film baru
Route::get('/cast/create', [CastController::class, 'create']);

//Route untuk insert data inputan ke database table cast
Route::post('cast', [CastController::class, 'store']);

//R => Read data
//Route untuk menampilkan semua data pada table cataegories di DB
Route::get('/cast', [CastController::class, 'index']);
Route::get('/cast/{id}', [CastController::class, 'show']);


//U => Update Data
//Route yang mengarah ke form edit data dengan param id
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
Route::put('/cast/{id}', [CastController::class, 'update']);

//D = Delete Data
Route::delete('cast/{id}', [CastController::class, 'destroy']);