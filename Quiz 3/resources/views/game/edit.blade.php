@extends('layouts.master')

@section('title')
    Halaman List Game
@endsection

@section('content')
    <form method="POST" action="/game/{{ $game->id }}">
        @csrf
        @method("PUT")
        <div class="form-group">
            <label>Game Name</label>
            <input type="text" class="form-control" value="{{ $game->name }}" name="name" aria-describedby="emailHelp">
        </div>
        @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Gameplay</label>
            <textarea name="gameplay" class="form-control" id="" cols="30" rows="10">{{ $game->gameplay }}</textarea>
        </div>
        @error('gameplay')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Developer Game</label>
            <input type="text" value="{{ $game->developer }}" class="form-control" name="developer" aria-describedby="emailHelp">
        </div>
        @error('developer')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Tahun</label>
            <input type="number" value="{{ $game->year }}" class="form-control" name="year" aria-describedby="emailHelp">
        </div>
        @error('year')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection