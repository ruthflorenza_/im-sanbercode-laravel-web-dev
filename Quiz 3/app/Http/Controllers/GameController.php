<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GameController extends Controller
{

    public function index()
    {
        $games = DB::table('game')->get();
 
        return view('game.index', ['game' => $games]);
    }

    public function create()
    {
        return view('game.create');
    }

    public function store(Request $request)   // Kode untuk menyimpan data baru dari form input
    {
        // Validasi data
        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required',
        ]);

        // Buat data baru menggunakan model dan simpan ke basis data
        // $newData = new DataModel();
        // $newData->name = $validatedData['name'];
        // $newData->email = $validatedData['email'];
        // $newData->save();

        DB::table('game')->insert([
            'name' => $request['name'],
            'gameplay' => $request['gameplay'],
            'developer' => $request['developer'],
            'year' => $request['year'],
        ]);

        // Berikan pesan notifikasi
        session()->flash('success', 'Data berhasil disimpan.');

        // Redirect ke halaman yang sesuai
        return redirect('/game');
    }

    public function show($id) // Kode untuk menampilkan detail data dengan ID tertentu
    {
        $game = DB::table('game')->where('id', $id)->first();
        $platforms = DB::table('platform')->where('game_id', $id)->get();
        
        return view('game.show', ['game' => $game, 'platforms' =>$platforms]);
    }

    public function edit($id)
    {
        $game = DB::table('game')->where('id', $id)->first();
        
        return view('game.edit', ['game' => $game]);
    }

    public function update(Request $request, $id) // Kode untuk memperbarui data dengan ID tertentu
    {
        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required',
        ]);

        DB::table('game')
              ->where('id', $id)
              ->update(
                [
                    'name' => $request['name'],
                    'gameplay' => $request['gameplay'],
                    'developer' => $request['developer'],
                    'year' => $request['year'],
                ]
            );
        return redirect('/game');
    }

    public function destroy($id) // Kode untuk menghapus data dengan ID tertentu
    {
        DB::table('game')->where('id', '=', $id)->delete();

        return redirect('/game');
    }

}
