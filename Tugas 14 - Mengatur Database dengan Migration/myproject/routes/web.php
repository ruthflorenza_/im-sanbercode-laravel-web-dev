<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/home', [HomeController::class, 'utama' ]);

Route::get('/register', [AuthController::class, 'form']);

Route::get('/signup', [AuthController::class, 'daftar']);

Route::get('/welcome', [AuthController::class, 'tampil']);

Route::post('/welcome', [AuthController::class, 'masuk']);

Route::get('/data-tables', function() {
    return view('page.data-table');
});

Route::get('/tables', function(){
    return view('page.table');
});

//testing template
// Route::get('/master', function(){
//     return view('layouts.master');
// });