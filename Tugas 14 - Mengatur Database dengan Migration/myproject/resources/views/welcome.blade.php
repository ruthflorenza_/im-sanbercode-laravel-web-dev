@extends('layouts.master')

@section('title')
Halaman Welcome
@endsection

@section('content')

    <h1>Selamat Datang {{$firstName}} {{$lastName}} !</h1>
    <h3>Terimakasih telah bergabung di Sanberbook. Social Media kita bersama!</h3>
@endsection