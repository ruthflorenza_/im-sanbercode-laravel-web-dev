@extends('layouts.master')

@section('title')
Halaman Pendaftaran
@endsection

@section('content')
    
    <form method="POST" action="/welcome"><br>
        @csrf
        <label>First name:</label><br>
        <input type="text" name="first name"><br><br>
        <label>Last name:</label><br>
        <input type="text" name="last name"><br><br>
        <label>Gender:</label><br>
        <input type="radio" name="Male">Male<br>
        <input type="radio" name="Female">Female<br>
        <input type="radio" name="Other">other<br>
        <br>

        <label>Nationality</label><br>
            <select name="nationalities">
                <option value="indonesian">Indonesian</option>
                <option value="malaysia">Malaysia</option>
                <option value="singapura">Singapura</option>
                <option value="thailand">Thailand</option>
            </select>
        <br><br>

        <label>Language Spoken:</label><br>
        <input type="checkbox" name="Bahasa Indonesia">
        <label>Bahasa Indonesia</label><br>
        <input type="checkbox" name="English">
        <label>English</label><br>
        <input type="checkbox" name="Other">
        <label>Other</label>
        <br><br>

        <label>Bio:</label><br>
        <textarea name="message" rows="10" cols="30"></textarea>
        <br><br>
        <input type="submit" value="submit">
    </form>
@endsection