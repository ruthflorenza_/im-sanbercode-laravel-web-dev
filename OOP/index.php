<?php
require_once ('Animal.php');
require_once ('Ape.php');
require_once ('Frog.php');


$animal = new Animal("Shaun");
echo "Name : " . $animal->name ."<br>"; 
echo "Legs: " . $animal->legs . "<br>"; 
echo "Cold Blooded: " . $animal->cold_blooded . "<br> <br>";


$sungokong = new Ape("Kera sakti");
echo "Name : " . $sungokong->name ."<br>"; 
echo "Legs: " . $sungokong->legs . "<br>"; 
echo "Cold Blooded: " . $sungokong->cold_blooded . "<br>";
$sungokong->yell("Auooo <br>");


$kodok = new Frog("buduk");
echo "Name : " . $kodok->name ."<br>"; 
echo "Legs: " . $kodok->legs . "<br>"; 
echo "Cold Blooded: " . $kodok->cold_blooded . "<br>";
$kodok->jump("Hop Hop");

?>